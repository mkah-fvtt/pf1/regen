import { CFG } from './config.mjs';
import { migrateItem } from './migration/migration.mjs';

let itemConfigTemplate;

/**
 * @param {ItemSheet} sheet
 * @param {JQuery<HTMLElement>} html
 * @param {object} options
 */
function injectItemConfig(sheet, [html]) {
	const item = sheet.item;

	// Ignore some item types
	if (['race', 'consumable', 'spell', 'class', 'container'].includes(item.type)) return;

	const ad = html.querySelector('.tab[data-tab="advanced"] div');
	if (!ad) return;

	const bRegen = item.hasItemBooleanFlag(CFG.FLAGS.regen),
		bFastheal = item.hasItemBooleanFlag(CFG.FLAGS.fastHeal),
		bBleed = item.hasItemBooleanFlag(CFG.FLAGS.bleed);

	// On-the-spot migration
	if (sheet.isEditable && (bRegen && !!item.getItemDictionaryFlag(CFG.FLAGS.regen)) ||
		(bBleed && !!item.getItemDictionaryFlag(CFG.FLAGS.bleed)) ||
		(bFastheal && !!item.getItemDictionaryFlag(CFG.FLAGS.fastHeal))) {
		return void migrateItem(item);
	}

	const templateData = {
		regen: {
			formula: item.getFlag(CFG.id, CFG.FLAGS.regen),
		},
		bleed: {
			formula: item.getFlag(CFG.id, CFG.FLAGS.bleed),
		},
		fastHeal: {
			formula: item.getFlag(CFG.id, CFG.FLAGS.fastHeal),
		},
	};

	const template = document.createElement('template');
	template.innerHTML = itemConfigTemplate(templateData);

	/**
	 * @param {Event} ev
	 */
	async function updateValue(ev) {
		ev.preventDefault();
		ev.stopImmediatePropagation();

		const el = ev.target;

		const flag = CFG.FLAGS[el.dataset.target];
		if (!flag) return;

		const value = el.value.trim();
		const updateData = { system: { flags: { boolean: {} } }, flags: { [CFG.id]: {} } };
		if (value) {
			updateData.flags[CFG.id][flag] = value;
			updateData.system.flags.boolean[flag] = true;
		}
		else {
			updateData.flags[CFG.id][`-=${flag}`] = null;
			if (!Number.isFinite(item.system.level))
				updateData.system.flags.boolean[`-=${flag}`] = null;
		}
		item.update(updateData);
	}

	template.content.querySelectorAll('input').forEach(el => {
		if (sheet.isEditable) {
			el.addEventListener('change', updateValue);
		}
		else {
			el.readOnly = true;
			el.disabled = true;
		}
	});

	ad.after(template.content);
}

Hooks.once('setup', () => {
	getTemplate(`modules/${CFG.id}/template/item-config.hbs`).then(t => itemConfigTemplate = t);
});

Hooks.on('renderItemSheet', injectItemConfig);
