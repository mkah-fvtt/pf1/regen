/**
 * TODO: Treat positive and negative values separately.
 *
 * @param {string} input
 * @param {object} cfg
 * @param {object} [debug] Object for containing debug information
 */
export const getRegen = (input, cfg) => {
	const result = {};
	input = input ?? '';
	result.input = input;
	if (input.length == 0) {
		result.nop = true;
		return 0;
	}

	const vs = [0];
	for (let value of input.split(';')) {
		value = value.trim();
		const match = cfg.ignoreRE.find(f => f.test(value));
		if (match) {
			result.ignored = { value, match };
			continue;
		}

		if (/[([]/.test(value)) continue;
		const rv = value.match(/(?<value>-?\d+)/);
		const regenValue = rv?.groups.value;
		if (regenValue) vs.push(parseInt(regenValue, 10));
		else console.error('Invalid:', value, result);
	}
	// Sort regen/fh parts and get the biggest
	const rg = Math.max(...vs);

	result.numbers = vs;
	result.final = rg;

	return result;
};

export class HoTSource {
	value = null;
	source = null;
	origin;
	details;

	parse(source, cfg, origin) {
		if (typeof source !== 'string') return;

		this.source = source;
		this.details = getRegen(source, cfg);
		this.value = this.details.final;
		this.origin = origin;
		return this.details;
	}
}

export class HealthValue {
	value;
	nonlethal;
	temp;
	max;

	constructor({ value = 0, nonlethal = 0, temp = 0, max = 0 } = {}) {
		this.value = value ?? 0;
		this.nonlethal = nonlethal ?? 0;
		this.temp = temp ?? 0;
		this.max = max ?? 0;
	}

	get effective() {
		return this.value + this.temp;
	}

	get valid() {
		return Number.isSafeInteger(this.value) && Number.isSafeInteger(this.nonlethal) && Number.isSafeInteger(this.temp);
	}
}

export class HealthDelta extends HealthValue { }

export class HealthState extends HealthValue {
	bleeding;

	dead;
}

export class ConditionState {
	bleeding;
	staggered;
}

export class HealthChange {
	#volume = 0;

	get volume() {
		return this.#volume;
	}

	get isActive() {
		return this.#volume !== 0;
	}

	adjust(value) {
		this.#volume += value;
	}

	increase(value) {
		this.#volume = Math.max(this.#volume, value);
	}
}

export class HealthChangeInstance {
	formula;

	value;

	origin;
	#doc;

	/**
	 * @param {string} formula
	 * @param {string} origin
	 * @param {DocWrapper} doc
	 */
	constructor(formula, origin, doc) {
		if (typeof formula === 'string')
			formula = formula.trim();

		this.#doc = doc;

		Object.defineProperties(this, {
			formula: {
				value: `${formula}`,
				enumerable: true,
			},
			origin: {
				value: origin,
				enumerable: true,
			},
			value: {
				value: typeof formula === 'number' ? formula : undefined,
				writable: true,
			},
		});
	}

	get possiblyValid() {
		switch (typeof this.formula) {
			case 'number':
				return true;
			case 'string':
				return this.formula.length > 0;
			default:
				return false;
		}
	}

	async parse() {
		if (typeof this.formula === 'string') {
			this.roll = await new Roll.defaultImplementation(this.formula, this.#doc?.rollData ?? {}).evaluate();
			this.value = this.roll.total;
		}
		else
			this.value = this.formula;

		return this;
	}

	get isActive() {
		return Number.isFinite(this.value) && this.value !== 0;
	}
}

export class Stabilization {
	roll;
	dc;
	stabilized = true;

	dcRoll = {
		roll: null,
		json: null,
	};

	get success() {
		return this.roll.total >= this.dc;
	}

	get check() {
		return this.roll.total;
	}

	async stabilize(actor) {
		if (actor.system.attributes.hp.value >= 0)
			this.stabilized = true;
		else {
			const cmData = await actor.rollAbilityTest('con', { skipDialog: true, chatMessage: false });
			this.roll = Roll.fromJSON(unescape(cmData.rolls[0]));
			this.stabilized = this.success;
		}
	}

	constructor(dc) {
		this.dc = dc;
	}
}
