export class RegenConfigData extends foundry.abstract.DataModel {
	static defineSchema() {
		const fields = foundry.data.fields;
		return {
			ignore: new fields.ArrayField(
				new fields.StringField({ blank: false, nullable: false }),
				{
					nullable: false,
					initial: [
						// /\(.*\)/, // ignore any with parenthesis, hopefully catching most instances with special cases.
						'per (hour|min|day)'
					]
				}
			),
			chatcard: new fields.BooleanField({ initial: true }),
			transparency: new fields.BooleanField({ initial: false }),
			autoBleed: new fields.BooleanField({ initial: false }),
			autoDefeat: new fields.BooleanField({ initial: false }),
			autoStabilize: new fields.BooleanField({ initial: false }),
			stabilizeCard: new fields.BooleanField({ initial: true }),
			debug: new fields.BooleanField({ initial: false }),
			migrated: new fields.NumberField({ integer: true, initial: 0 }),
		}
	}
}
