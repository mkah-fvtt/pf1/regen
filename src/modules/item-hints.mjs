// Compatibility with Item Hints

import { CFG } from '../config.mjs';

let itemHintsAPI;

/**
 * @param {ActorSheet} actorSheet
 * @param {Item} item
 * @param {object} data
 * @returns {undefined|Hint[]}
 */
function itemHintsHandler(actorSheet, item, data) {
	const bRegen = item.hasItemBooleanFlag(CFG.FLAGS.regen),
		bBleed = item.hasItemBooleanFlag(CFG.FLAGS.bleed),
		bFastHeal = item.hasItemBooleanFlag(CFG.FLAGS.fastHeal);

	if (!bRegen && !bBleed && !bFastHeal) return;

	const itemData = item.system;
	const rawRegen = bRegen ? (item.getFlag(CFG.id, CFG.FLAGS.regen) ?? itemData.level) : null;
	const rawBleed = bBleed ? (item.getFlag(CFG.id, CFG.FLAGS.bleed) ?? itemData.level) : null;
	const rawFastHeal = bFastHeal ? (item.getFlag(CFG.id, CFG.FLAGS.fastHeal) ?? itemData.level) : null;

	const cls = itemHintsAPI.HintClass;
	const hints = [];
	if (rawRegen) hints.push(cls.create(game.i18n.localize('HealthOverTime.ItemHint.Regen'), ['regen'], { hint: game.i18n.format('HealthOverTime.ItemHint.RegenHint', { formula: rawRegen }), icon: ['fas', 'fa-heartbeat'] }));
	if (rawFastHeal) hints.push(cls.create(game.i18n.localize('HealthOverTime.ItemHint.FastHeal'), ['regen'], { hint: game.i18n.format('HealthOverTime.ItemHint.FastHealHint', { formula: rawFastHeal }), icon: ['fas', 'fa-heartbeat'] }));
	if (rawBleed) hints.push(cls.create(game.i18n.localize('HealthOverTime.ItemHint.Bleed'), ['bleed'], { hint: game.i18n.format('HealthOverTime.ItemHint.BleedHint', { formula: rawBleed }), icon: ['fas', 'fa-tint'] }));
	return hints;
}

Hooks.once('ready', () => {
	const itemHintsModule = game.modules.get('mkah-pf1-item-hints');
	if (itemHintsModule?.active) {
		itemHintsAPI = itemHintsModule.api;
		itemHintsAPI.addHandler(itemHintsHandler);
	}
});
