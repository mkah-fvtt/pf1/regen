import { CFG } from './config.mjs';
import { getUsers } from './common.mjs';

/**
 * Combat and prettify chat messages.
 *
 * @param {ChatMessage} cm
 * @param {JQuery} jq
 */
export function compactMessage(cm, jq) {
	// const settings = getSettings();
	const flags = cm.flags?.[CFG.id];
	if (!flags) return;
	const html = jq[0];

	const wt = html.querySelector('.whisper-to');
	if (wt) wt.style.display = 'none'; // Remove whisper target

	const sc = html.closest('.chat-message');
	if (!sc) return;
	sc.classList.add('koboldworks', 'health-over-time');
	if (flags.regeneration) sc.classList.add('regeneration');
	if (flags.stabilization) sc.classList.add('stabilization');
	if (flags.bleedout) sc.classList.add('bleedout');

	// Metadata
	const mh = sc.querySelector('.message-header');
	if (mh) {
		const ts = mh.querySelector('.message-timestamp');
		mh.classList.remove('message-header');
		if (ts) ts.style.display = 'none'; // Hide
	}

	// Move contents
	const ms = html.querySelector(`.${CFG.id}`);
	const sender = mh.querySelector('.message-sender');
	if (sender) {
		sender.classList.remove('message-sender');
		sender.classList.add('subject');
	}

	// Shuffle
	mh?.after(ms);

	html.style.borderColor = '';
}

/**
 * @param {import("./data/state-data.mjs").HealthOverTimeState} d
 * @param {import("./data/regen-config.mjs").RegenConfigData} cfg
 */
export const generateBaseCard = (d, cfg) => ({
	style: CONST.CHAT_MESSAGE_STYLES.OOC,
	rollMode: cfg.transparency ? CONST.DICE_ROLL_MODES.PUBLIC : CONST.DICE_ROLL_MODES.PRIVATE,
	speaker: ChatMessage.getSpeaker({ actor: d.actor, token: d.token?.document, alias: d.token?.name }),
	whisper: cfg.transparency ? [] : getUsers(d.actor, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER),
	flags: { [CFG.id]: {} },
});

/**
 * TODO: Allow overriding this somehow?
 *
 * @param {import("./data/state-data.mjs").HealthOverTimeState} d
 * @param {import("./data/regen-config.mjs").RegenConfigData} cfg
 */
export const generateCard = async (d, cfg) => {
	d.card.displayName = d.token.name;
	const cardData = generateBaseCard(d, cfg);
	cardData.flags[CFG.id].regeneration = true;
	cardData.content = await renderTemplate(CFG.cardTemplate, d);
	return cardData;
};

/**
 * @param {import("./data/state-data.mjs").HealthOverTimeState} d
 * @param {import("./data/regen-config.mjs").RegenConfigData} cfg
 */
export const generateStabilizationCard = async (d, cfg) => {
	d.stabilization.json = escape(JSON.stringify(d.stabilization.roll));
	/* global RollPF */
	d.stabilization.dcRoll.roll = RollPF.safeRoll(`10[Base] - ${-(d.stabilization.dc - 10)}[Health]`);
	d.stabilization.dcRoll.json = escape(JSON.stringify(d.stabilization.dcRoll.roll));
	const data = generateBaseCard(d, cfg);
	data.flags[CFG.id].stabilization = true;
	data.content = await renderTemplate(CFG.stabilizationTemplate, d);
	return data;
};

/**
 * @param {import("./data/state-data.mjs").HealthOverTimeState} d
 * @param {import("./data/regen-config.mjs").RegenConfigData} cfg
 */
export const generateBleedoutCard = (d, cfg) => {
	const message = game.i18n.localize('HealthOverTime.BledOut');
	const data = generateBaseCard(d, cfg);
	data.flags[CFG.id].bleedout = true;
	data.content = '<div>' + message + '</div>';
};
