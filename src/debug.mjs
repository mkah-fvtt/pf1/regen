import { CFG } from './config.mjs';
import { signNum } from './common.mjs';

export const printRegenLog = (actor, d, settings) => {
	const output = [
		`${CFG.label} | ${actor.name} [${actor.id}] changes: `,
		// Health
		`${signNum(d.delta.value)} HP ` +
		game.i18n.format('HealthOverTime.HPChange', { old: d.past.value, new: d.post.value }),
		// Nonlethal
		`; ${signNum(-d.delta.nonlethal)} NL ` +
		game.i18n.format('HealthOverTime.HPChange', { old: d.past.nonlethal, new: d.post.nonlethal }),
		// Temp
		`; ${signNum(-d.delta.temp)} THP ` +
		game.i18n.format('HealthOverTime.HPChange', { old: d.past.temp, new: d.post.temp }),
	];

	const bled = d.past.bleeding !== d.conditions.bleeding;
	if (d.bleed.value.isActive) output.push(`; Bled ${d.bleed.value.volume}`);
	if (bled) output.push('; ' + game.i18n.localize('HealthOverTime.Bleed.Stopped'));

	if (d.conditions.bleeding) output.push('; ' + game.i18n.localize('HealthOverTime.Bleed.Ongoing'));
	if (d.dead) output.push('; ' + game.i18n.localize('HealthOverTime.Defeated'));

	// Combine output and print to console
	console.log(output.filter(i => i.length).join(''), '\n', d);
};
