import { CFG } from '../config.mjs';

function limitPrecision(number, decimals = 2) {
	const mult = Math.pow(10, decimals);
	return Math.floor(number * mult) / mult;
}

/**
 * @param {Item} item
 */
export async function migrateItem(item) {
	const bRegen = item.hasItemBooleanFlag(CFG.FLAGS.regen);
	const bBleed = item.hasItemBooleanFlag(CFG.FLAGS.bleed);
	const bFastHeal = item.hasItemBooleanFlag(CFG.FLAGS.fastHeal);
	if (!bRegen && !bBleed && !bFastHeal) return null;

	const dRegen = item.getItemDictionaryFlag(CFG.FLAGS.regen);
	const dBleed = item.getItemDictionaryFlag(CFG.FLAGS.bleed);
	const dFastHeal = item.getItemDictionaryFlag(CFG.FLAGS.fastHeal);
	if (dRegen == undefined && dBleed == undefined && dFastHeal == undefined) return null;

	const updateData = { system: { flags: { dictionary: {} } }, flags: { [CFG.id]: {} } };
	if (bRegen && dRegen) {
		updateData.system.flags.dictionary[`-=${CFG.FLAGS.regen}`] = null;
		updateData.flags[CFG.id][CFG.FLAGS.regen] = dRegen;
	}

	if (bBleed && dBleed) {
		updateData.system.flags.dictionary[`-=${CFG.FLAGS.bleed}`] = null;
		updateData.flags[CFG.id][CFG.FLAGS.bleed] = dBleed;
	}

	if (bFastHeal && dFastHeal) {
		updateData.system.flags.dictionary[`-=${CFG.FLAGS.fastHeal}`] = null;
		updateData.flags[CFG.id][CFG.FLAGS.fastHeal] = dFastHeal;
	}

	console.debug(CFG.label, '| Migrating |', item.name, item.uuid);
	return item.update(updateData);
}

export async function migrateActor(actor, state = {}) {
	for (const item of actor.items) {
		try {
			const rv = await migrateItem(item);
			if (rv) state.migrated += 1;
		}
		catch (err) {
			console.error(CFG.label, '| Migration |', item.uuid, '| Error:', err);
		}
	}
}

export async function migrateWorld() {
	const actors = [...game.actors];
	console.log(CFG.label, '| Migration | Actors |', actors.length);
	let pct10 = Math.max(25, Math.floor(actors.length / 10));
	if (actors.length < 50) pct10 = 100;
	let i = 0;
	const state = { migrated: 0 };
	for (const actor of actors) {
		if (!actor.isOwner) continue;

		i += 1;
		if (i % pct10 === 0) console.log(CFG.label, '| Migration |', limitPrecision((i / actors.length) * 100, 1), '% |', i, 'out of', actors.length);
		await migrateActor(actor, state);
	}

	/** @type {Scene[]} */
	const scenes = [...game.scenes];
	console.log(CFG.label, '| Migration | Scenes |', scenes.length);
	i = 0;
	pct10 = Math.max(3, Math.floor(scenes.length / 10));
	for (const scene of scenes) {
		i += 1;
		if (i % pct10 === 0) console.log(CFG.label, '| Migration |', limitPrecision((i / actors.length) * 100, 1), '% |', i, 'out of', scenes.length);
		/** @type {TokenDocument[]} */
		const tokens = scene.tokens;
		for (const token of tokens) {
			if (!token.actorId) continue;
			if (token.isLinked) continue;

			const actor = token.actor;
			if (!actor) continue;

			await migrateActor(actor, state);
		}
	}

	const { migrated } = state;
	console.log(CFG.label, '| Migration | Complete |', migrated, 'Items Adjusted');
	const mod = game.modules.get(CFG.id);
	game.settings.set(CFG.id, 'migration', mod.version);
}

Hooks.once('pf1PostReady', async () => {
	if (!game.users.activeGM?.isSelf) return;

	const migration = game.settings.get(CFG.id, 'migration');
	const mod = game.modules.get(CFG.id);
	if (migration === mod.version || isNewerVersion(migration, mod.version)) return void console.debug(CFG.label, '| Migration | Not needed');

	console.log(CFG.label, `| Migration | ${migration} -> ${mod.version}`);

	migrateWorld();
});
