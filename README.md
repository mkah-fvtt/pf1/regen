# Health over Time for Pathfinder 1e

[![Latest Release](https://gitlab.com/koboldworks/pf1/health-over-time/-/badges/release.svg)](https://gitlab.com/koboldworks/pf1/health-over-time/-/releases)

Adds automated regeneration, fast healing, and bleeding.

Regeneration automatically stops bleeding condition.

Chat example:  
![PF1 Regen In Chat](./img/screencaps/chat.png)

Console log example:  
![PF1 Regen In Logs](./img/screencaps/log.png)

Optionally automatically stabilizes as well.

## Configuration

### Simple

Simple configuration is available in advanced tab.

### Advanced

Items can be configured by including `bleed`, `regen`, `fastHeal` or `dying` flags to them. Boolean flag is used on actor level to detect if any items have them without needing to scan over all items and to control item level level scaling.

Any item that understands being active (.isActive can return true) is viable for holding this information.

#### **Bleeding**

Requires _boolean_ flag: `bleed`

If no specific formula is provided, _item level_ is used instead.

Only largest bleeding source is applied.

#### **Regen**

Requires _boolean_ flag: `regen`

If no specific formula is provided, _item level_ is used instead.

Only largest regen source is applied.

#### **Fast Heal**

Requires _boolean_ flag: `fastHeal`

If no specific formula is provided, _item level_ is used instead.

Only largest fast healing source is applied.

#### **Dying**

Requires _boolean_ flag: `dying`

Causes 1 point of bleed for as long as the buff is active.

This does not stack with bleeding, only largest source is applied.

## API

```js
const hotpi = game.modules.get("pf1-health-over-time").api;
hotpi.migration.actor(game.actors.getName("Bob")); // Migrate specific actor
hotpi.migration.item(game.items.getName("Bloody Mess")); // Migrate specific item
hotpi.migraiton.world(); // Migrate all actors and items
```

## Limitations

- You may have to undo bleed in some circumstances since the system is not perfect.

## Install

Manifest URL: <https://gitlab.com/koboldworks/pf1/health-over-time/-/releases/permalink/latest/downloads/module.json>

Requirements:

- Foundry v12
- Pathfinder 1e v11

### Old unsupported versions

Last compatible versions:

- _Foundry v10 & v11_: <https://gitlab.com/koboldworks/pf1/health-over-time/-/releases/0.6.0.5/downloads/module.json>

- _Foundry v9_: <https://gitlab.com/koboldworks/pf1/health-over-time/-/releases/0.5.0.3/downloads/module.json>

- _Foundry v0.8_: <https://gitlab.com/koboldworks/pf1/health-over-time/-/raw/0.3.0.4/module.json>

## Attribution

If you use any of the code in this project, I would appreciate I or the project was credited for inspiration or whatever where appropriate. Or just drop a line about using my code. I do not mind not having this, but it's just nice knowing something has come out of my efforts.

## Donations

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/I2I13O9VZ)

## License

This software is distributed under the [MIT License](./LICENSE).
