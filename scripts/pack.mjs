import fs from 'node:fs';
import path from 'node:path';
import process from 'node:process';

import { compilePack, extractPack } from '@foundryvtt/foundryvtt-cli';

const compileOptions = {
	yaml: true,
	yamlOptions: {
		sortKeys: true, // Prevent random key drift
	},
};

const srcPath = 'packs';
const livePath = 'dist/packs';

// Package JSON
const json = JSON.parse(fs.readFileSync('static/module.json'));

const args = process.argv.slice(2);
const focus = new Set();
let doPacking = false, doUnpacking = false;
while (args.length) {
	const arg = args.shift();
	switch (arg) {
		case '--pack': doPacking = true; break;
		case '--unpack': doUnpacking = true; break;
		default: focus.add(arg);
	}
}

console.log('Focus:', focus);

const packs = json.packs.map(p => p.name).filter(p => {
	if (focus.size) return focus.has(p);
	return true;
});

console.log('Workset:', packs);

function getPackData(pack) {
	return json.packs.find(i => i.name === pack);
}

/*
const slugify = (name) => {
	return name.trim()
		.replace(/[\s|.?\\/:*"><]+/g, '-'); // OS unsafe characters
}
*/

async function compileTransform(pack, data) {
	// console.log('compileTransform:', pack, data.name, data);
}

/**
 * @param {object} json
 */
function cleanCommonDocumentData(json) {
	delete json._stats;
	delete json.ownership;

	if (json.flags) {
		delete json.flags.core;
	}

	if (json.system && Object.keys(json.system).length === 0) delete json.system;

	if (json.flags && Object.keys(json.flags).length === 0) delete json.flags;
}

function cleanJournalData(json) {
	for (const page of (json.pages ?? [])) {
		cleanCommonDocumentData(page);
		delete page.video;
		if (!page.src) delete page.src;
		if (page.image && Object.keys(page.image)?.length === 0) delete page.image;
	}
}

/**
 * @param {string} pack
 * @param {object} json
 */
async function unpackTransform(pack, json) {
	const pd = getPackData(pack);
	const isJournal = pd.type === 'JournalEntry';

	cleanCommonDocumentData(json);
	if (json.folder === null) delete json.folder;
	if (json.effects?.length === 0) delete json.effects;
	if (json.sort === 0) delete json.sort;

	if (json.system.changes?.length == 0) delete json.system.changes;
	if (json.system.tags?.length == 0) delete json.system.tags;
	if (json.system.actions?.length == 0) delete json.system.actions;
	if (json.system.attackNotes?.length == 0) delete json.system.attackNotes;
	if (json.system.effectNotes?.length == 0) delete json.system.effectNotes;
	if (json.system.contextNotes?.length == 0) delete json.system.contextNotes;
	if (json.system.scriptCalls?.length == 0) delete json.system.scriptCalls;

	if (json.system.flags?.dictionary) {
		if (Object.keys(json.system.flags.dictionary).length === 0) delete json.system.flags.dictionary;
	}

	delete json.system.links;
	delete json.system.description?.unidentified;
	if (!json.system.description?.instructions) delete json.system.description?.instructions;
	if (!json.system.showInQuickbar) delete json.system.showInQuickbar;
	delete json.system.changeFlags;
	delete json.system.uses;
	delete json.flags?.pf1;
	if (json.flags) {
		for (const key of Object.keys(json.flags)) {
			if (key !== 'pf1-health-over-time') delete json.flags[key];
		}
	}

	if (!(json.system.conditions?.length > 0)) delete json.system.conditions;

	if (Object.keys(json.flags ?? {}).length == 0) delete json.flags;

	if (json.system.level == null) delete json.system.level;
	if (!json.system.tag) delete json.system.tag;

	if (!json.system.duration?.units) delete json.system.duration;

	if (isJournal) cleanJournalData(json);
}

/**
 * @param {string} pack - Pack name
 * @param {object} json - Pack JSON data
 * @returns {string} - New file name
 */
async function unpackRename(pack, json) {
	const { name, _id: id } = json;
	const ext = compileOptions.yaml ? 'yaml' : 'json';
	const nname = `${name}.${id}.${ext}`;
	return nname;
}

async function packCompendiums() {
	for (const pack of packs) {
		const inpath = path.join(srcPath, pack);
		const outpath = path.join(livePath, pack);
		console.log('Packing:', pack);
		await compilePack(inpath, outpath, { transformEntry: (json) => compileTransform(pack, json), ...compileOptions });
	}
}

async function unpackCompendiums() {
	for (const pack of packs) {
		const inpath = path.join(livePath, pack);
		const outpath = path.join(srcPath, pack);
		console.log('Unpacking:', pack);
		await extractPack(inpath, outpath, {
			transformEntry: (json) => unpackTransform(pack, json),
			transformName: (json) => unpackRename(pack, json),
			...compileOptions,
		});
	}
}

if (doUnpacking) await unpackCompendiums();
else if (doPacking) await packCompendiums();
